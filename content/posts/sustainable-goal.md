+++ 
draft = false
date = 2020-03-30T10:02:31+03:00
title = "Name a Sustainable Development Goal"
description = "Name a Sustainable Development Goal"
slug = "sustainable-goal"
tags = []
categories = []
externalLink = ""
series = []
+++

Regarding the list of sustainable development goals on [UN’s website](https://sustainabledevelopment.un.org/sdgs), **_#4 - Quality Education_** is the most important to me. I believe, without quality education you cannot grow self-empowered individuals. And without self-empowered individuals you cannot build a self-empowered society/nation/country. If a society does not have sufficient amount of well-educated human population, any sustainable development efforts from outside actually will not be sustainable in the end.

You can give money to people in need, but if they’re not educated they will probably spend it unwisely and they’ll need more.

You can give free medical care to people in need, but if they’re not educated they will probably won’t take the responsibility of their own health and they’ll be sick again soon.

You can dig water wells for people who need fresh water, but if they’re not educated they will probably waste or contaminate clean water sources and it won’t solve the problem.

I don’t say that other topics are not important. Of course they are important. Without clean water and food you cannot talk about education. But I believe, without quality education all other efforts will fail to remain sustainable.

So I think **_BASF Türk Kimya_** did a very good job with their YouTube project [Basfi ile Deneysel Bilim](https://www.youtube.com/channel/UCnAlXnZRNikus5wf4BPu4_A/about). Videos are entertaining as well as they’re educating. I, as a kid back in the 90s, was trying to make experiments I saw in a middle school science book with the materials I found at home. A compass made from an eraser and a razor blade, a thermometer made from an empty drug tube and a lollipop stick just to name a few. I would feel very lucky if I had that YouTube channel back in those days.

Also, we’re all passing through difficult and extraordinary times these days because of the Coronavirus outbreak, where online and schoolless education becomes not just important but also inevitable. It seems online education will become more and more common and important in the very near future.
