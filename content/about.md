+++
title = "About Me"
slug = "about"
+++

Hi, I am **Ahmet Ardal**!

I'm building **mobile** apps since **2010**.

I'm currently **the CTO of [Leo AR](https://leoapp.com/)**, a company that allows you to redefine the world around you with Augmented Reality.
