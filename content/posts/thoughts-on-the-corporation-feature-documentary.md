+++ 
draft = false
date = 2020-03-18T10:18:51+03:00
title = "Thoughts on The Corporation – Feature, Documentary"
description = "Thoughts on The Corporation – Feature, Documentary"
slug = "thoughts-on-the-corporation-feature-documentary"
tags = []
categories = []
externalLink = ""
series = []
+++

**_[The Corporation](https://www.imdb.com/title/tt0379225/)_** tells the story of corporations by investigating the story and evolution of corporations throughout the history of America. Since the United States is the home of almost all big inventions, revolutions and changes of the last two centuries, it’s very logical and meaningful to do so.

The film starts by explaining how the modern corporation was born and how corporations are being recognised as a person-like entity. It is also described as **_[Corporate personhood](https://en.wikipedia.org/wiki/Corporate_personhood)_** and it is still a hot debate whether corporations are people or not. We have a similar term in Turkish as well: **_“Tüzel Kişi”_**. I’m not sure if a corporation should be treated as a person or not by law and governments but I did like the analogy because I always tend to think it this way: The corporation is simply an organised unit, so are we humans. I’ll come to this later.

The film continues by telling how corporations become selfish creatures by only looking after their own benefits and profits at the cost of other human’s health and lives, other creatures’ lives and the sustainability and future of our planet. When the profit and money becomes the only goal the modern corporation sees everything as a money making tool, whether it is a human, whether it is a river, whether it is a forest, whether it is a cow, whether it is a pig or chicken.

Many famous big corporations are exploiting the difficult circumstances of the poor people by making them work for incredibly low hourly rates and under very bad work conditions given the fact that they don’t have any other chance. So instead of starving to death they work for them. These same corporations are also buying the rights to manage main infrastuctures in those poor countries such as city water and electricity so the people often have no chance to grow their own food and live sustainably on their own. Corporations own the machines, corporations own the oil that operates the machines, corporations own the technology, corporations own the medical industry and the worst part is corporations own the media which is the source of truth for many people. They own the governments and the police. So in every aspect it’s a very cleverly set trap that poor and unconscious masses have no chance to escape.

Corporations did and continue doing very bad things, I agree with that. But they cannot be separated from us, human beings. Every single bad thing that a corporation does is being decided and executed by a human being. Separating it from us creates a devil-like illusion. When we create that separation it becomes impossible to address and heal it. When every one of us -the humankind- become aware of other humans, creatures and Mother Earth then the corporations will become aware of humans, creatures and Mother Earth. They are not separate creatures. They’re us.
