## Update footer copyright year

```
git commit --allow-empty -m "Update footer year to 2025"
git push -u origin master
```
